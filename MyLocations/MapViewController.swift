//
//  MapViewController.swift
//  MyLocations
//
//  Created by Hui Chih Wang on 2020/12/22.
//

import UIKit
import MapKit
import CoreData

class MapViewController: UIViewController {
    @IBOutlet var mapView: MKMapView!
    
    var locations = [Location]()
    
    var managedObjectContext: NSManagedObjectContext! {
        didSet {
            NotificationCenter.default.addObserver(
                forName: Notification.Name.NSManagedObjectContextObjectsDidChange,
                object: managedObjectContext,
                queue: OperationQueue.main
            ) { notification in
                if self.isViewLoaded {
                    
//                    if let dictionary = notification.userInfo {
//                        print("dictionary[NSInsertedObjectsKey]: \(dictionary[NSInsertedObjectsKey])")
//                        print("dictionary[NSUpdatedObjectsKey]: \(dictionary[NSUpdatedObjectsKey])")
//                        print("dictionary[NSDeletedObjectsKey]: \(dictionary[NSDeletedObjectsKey])")
//                    }
                    
                    self.setInitialLocations()
                }
            }
        }
    }
    
    var region: MKCoordinateRegion {
        let region: MKCoordinateRegion
        switch locations.count {
        case 0:
            region = MKCoordinateRegion(
                center: mapView.userLocation.coordinate,
                latitudinalMeters: 1000,
                longitudinalMeters: 1000)
            
        case 1:
            region = MKCoordinateRegion(
                center: locations.first!.coordinate,
                latitudinalMeters: 1000,
                longitudinalMeters: 1000)
            
        default:
            var topLeft = CLLocationCoordinate2D(latitude: -90, longitude: 180)
            var bottomRight = CLLocationCoordinate2D(latitude: 90, longitude: -180)
            locations.forEach { annotation in
                topLeft.latitude = max(topLeft.latitude, annotation.coordinate.latitude)
                topLeft.longitude = min(topLeft.longitude, annotation.coordinate.longitude)
                bottomRight.latitude = min(bottomRight.latitude, annotation.coordinate.latitude)
                bottomRight.longitude = max(bottomRight.longitude, annotation.coordinate.longitude)
            }
            
            let center = CLLocationCoordinate2D(
                latitude: topLeft.latitude - (topLeft.latitude - bottomRight.latitude) / 2,
                longitude: topLeft.longitude - (topLeft.longitude - bottomRight.longitude) / 2)
            
            let extraSpace = 1.7
            let span = MKCoordinateSpan(
                latitudeDelta: abs(topLeft.latitude - bottomRight.latitude) * extraSpace,
                longitudeDelta: abs(topLeft.longitude - bottomRight.longitude) * extraSpace)
            
            region = MKCoordinateRegion(center: center, span: span)
        }
        
        return mapView.regionThatFits(region)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialLocations()
        if !locations.isEmpty {
            showLocations()
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EditLocation" {
            let controller = segue.destination as! LocationDetailsViewController
            controller.managedObjectContext = managedObjectContext
            
            let button = sender as! UIButton
            let location = locations[button.tag]
            controller.locationToEdit = location
        }
    }
    
    // MARK: - Actions
    @IBAction func showUser() {
        let region = MKCoordinateRegion(center: mapView.userLocation.coordinate, latitudinalMeters: 1000,longitudinalMeters: 1000)
        mapView.setRegion(
            mapView.regionThatFits(region),
            animated: true)
    }
    
    @IBAction func showLocations() {
        mapView.setRegion(region, animated: true)
    }
    
    @objc func showLocationDetails(_ sender: UIButton) {
        performSegue(withIdentifier: "EditLocation", sender: sender)
    }
    
    
    // MARK: - Helper methods
    func setInitialLocations() {
        mapView.removeAnnotations(locations)
        
        let entity = Location.entity()
        
        let fetchRequest = NSFetchRequest<Location>()
        fetchRequest.entity = entity
        
        locations = try! managedObjectContext.fetch(fetchRequest)
        mapView.addAnnotations(locations)
    }
    
    /*
    func updateLocations(notification: Notification) {
        let updateInfo = notification.userInfo
        
        if let removeLocations = updateInfo?[NSDeletedObjectsKey] {
            let removeAnnotations = Array(removeLocations as! Set<Location>)
            mapView.removeAnnotations(removeAnnotations)
            
        }
        
        if let updateLocations = updateInfo?[NSUpdatedObjectsKey] {
            let updateAnnotations = updateLocations as! Set<Location>
            mapView.removeAnnotations(Array(updateAnnotations))
            mapView.addAnnotations(Array(updateAnnotations))
        }
        
        if let insertLocations = updateInfo?[NSInsertedObjectsKey] {
            let insertAnnotations = insertLocations as! Set<Location>
            mapView.addAnnotations(Array(insertAnnotations))
        }
    }
 */
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard annotation is Location else {
            return nil
        }
        
        let identifier = "Location"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            let pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            pinView.isEnabled = true
            pinView.canShowCallout = true
            pinView.animatesDrop = false
            pinView.pinTintColor = UIColor(red: 0.32, green: 0.82, blue: 0.4, alpha: 1)
            
            let rightButton = UIButton(type: .detailDisclosure)
            rightButton.addTarget(self, action: #selector(showLocationDetails(_:)), for: .touchUpInside)
            pinView.rightCalloutAccessoryView = rightButton
            
            annotationView = pinView
        }
        
        if let annotationView = annotationView {
            annotationView.annotation = annotation
            
            let button = annotationView.rightCalloutAccessoryView as! UIButton
            if let index = locations.firstIndex(of: annotation as! Location) {
                button.tag = index
            }
        }
        
        return annotationView
    }
    
}
