//
//  String+AddText.swift
//  MyLocations
//
//  Created by Hui Chih Wang on 2020/12/27.
//

import Foundation
import CoreLocation

extension String {
    mutating func add(text: String?, separatedBy separator: String) {
        if let text = text {
            let newText = text + (text.isEmpty ? "" : separator)
            self += newText
        }
    }
}

extension CLPlacemark {
    enum StringFormat {
        case oneLine
        case twoLine
    }
    
    func string(format: StringFormat) -> String {
        var address = ""
        
        switch format {
        case .twoLine:
            address.add(text: self.subThoroughfare, separatedBy: "")
            address.add(text: self.thoroughfare, separatedBy: " ")

            var line2 = ""
            line2.add(text: self.locality, separatedBy: "")
            line2.add(text: self.administrativeArea, separatedBy: " ")
            line2.add(text: self.postalCode, separatedBy: " ")

            address.add(text: line2, separatedBy: "\n")
        case .oneLine:
            address.add(text: self.subThoroughfare, separatedBy: "")
            address.add(text: self.thoroughfare, separatedBy: " ")
            address.add(text: self.locality, separatedBy: ", ")
        }
        
        return address
    }
}
